var axe = require("../../node_modules/axe-core/axe.js");

document.write("It works.");

axe.run(document, function (results) {
   console.log('axerun1');
});

axe.run(function (err, results) {
   console.log('axerun2');
   if (err) throw err;
   ok(results.violations.length === 0, 'Should be no accessibility issues');
});