const HtmlWebPackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require("path");

module.exports = {
   entry: ["./src/scripts/main.js", "./src/styles/main.scss"],
   output: {
      filename: "[name].js",
      path: path.join(__dirname, "./build/")
   },
   module: {
      rules: [
         {
            test: /\.html$/,
            use: [{ loader: "html-loader", options: { minimize: true } }]
         },
         {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
               use: [
                  {
                     loader: "css-loader",
                     options: { minimize: true }
                  },
                  { loader: "postcss-loader" },
                  { loader: "sass-loader" }
               ]
            })
         },
         {
            test: /\.js$/,
            use: {
               loader: "babel-loader",
               options: {
                  presets: ["env"]
               }
            }
         }
      ]
   },
   plugins: [
      new HtmlWebPackPlugin({
         template: "./src/static/index.html",
         filename: "./index.html"
      }),
      new ExtractTextPlugin({
         filename: "css/main.css"
      })
   ],
   devServer: {
      contentBase: "./build"
   }
};